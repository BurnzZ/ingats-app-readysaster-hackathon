<?php

require ("../db/Database.php");
require ("parse.php");
require ('../back_access/globeAPI/src/GlobeApi.php');


function onDataFetched($data){
	echo "Data acquired!" . PHP_EOL . PHP_EOL;
	echo "STORM NAME: " . $data['name'] . PHP_EOL;
	echo "DATA LENGTH: " . count($data['track']) . PHP_EOL . PHP_EOL;

	broadcastCycloneMessage($data);
}

//broadcast a string message to all subscribed users
function broadcast($message){//broadcast custom message to all users
	$subscribers = $db->getSubscribers();
	foreach ($subscribers as $subscriber){
		sendMessage($subscriber,$message);
	}
}

//broadcast a cyclone warning message customized for each user
function broadcastCycloneMessage($data){
	$db = new Database();

	$constants = array(
		'Tropical Depression' => array (
			'max_wind_speed' => '63',
			'threshold_radius' => '150'
		),

		'Tropical Storm' => array (
			'max_wind_speed' => '118',
			'min_wind_speed' => '63',
			'threshold_radius' => '200'
		),

		'Typhoon' => array (
			'min_wind_speed' => '118',
			'threshold_radius' => '250'
		)
	);

	$hit = 0;

	
	echo "Computing Trajectory Hits..." . PHP_EOL;
	// expounds the abbreviation of the storm type
	$cyclone = getLatestCycloneTrackEntry($data);
	$subscribers = $db->getSubscribers();
	$type = getCycloneFullType($cyclone['type']);
	// figures out if system should alert subscriber according to their relative distance to the stormeye
	foreach ($subscribers as $subscriber){
		// compute distance of subscriber to the stormeye according to the latest forecast by PAGASA
		$distance = getDistance($subscriber['longitude'], $subscriber['latitude'], $cyclone);

		// if ($distance < $constants[$type]['threshold_radius']) {
			$message = composeCycloneReportMessage($cyclone,$subscriber);
			sendMessage($subscriber,$message);
			$hit++;
		// }
	}

	echo "Hit " . $hit . " out of " . count($subscribers);

}

//compose a simplified cyclone warning message base on cyclone and subscriber info
function composeCycloneReportMessage($cyclone, $subscriber){
	$distance = getComputedInfo($cyclone, $subscriber);
	/*
		eg.
		Tropical Storm Domeng is now 100km (storm eye) from your registered location. 
		Traveling with windspeeds from 63 to 118kph. -- As of 2014-04-06 14:00.
	*/

	$rounded_distance = round($distance,2);
	$message = "{$cyclone['type_full']} {$cyclone['name']} is now {$rounded_distance} km from {$subscriber['location']} as of {$cyclone['date']} {$cyclone['time']}.";
	return $message;
}

//at the moment it only gets the distance but this function is for retrieving an array of related derived values
function getComputedInfo($cyclone, $subscriber){
	$computed = getDistance($subscriber['longitude'], $subscriber['latitude'], $cyclone);

	return $computed;
}

//get the latest cyclone entry from the data made available by pag-asa
function getLatestCycloneTrackEntry($data){
	$track = $data['track'];
	$entry = $track[count($track) - 1]; //$latest_entry

	//$cyclone[] -> latest cyclone entry
	$cyclone['name'] = $data['name'];
	$cyclone['type'] = $entry['type'];
	$cyclone['date'] = $entry['date'];
	$cyclone['time'] = $entry['time'];
	$cyclone['longitude'] = $entry['long'];
	$cyclone['latitude'] = $entry['lat'];
	$cyclone['type_full'] = getCycloneFullType($cyclone['type']);

	return $cyclone;
}

//get full name of a cyclone type/category warning
function getCycloneFullType($type){
	$type_full = "";
	switch($type){
		case "TD":
			$type_full = "Tropical Depression";
			break;
		case "TS":
			$type_full = "Tropical Storm";
			break;
		case "TY":
			$type_full = "Typhoon";
			break;
	}

	return $type_full;
}

//send a text message to a specific subscriber
function sendMessage($subscriber,$message){
	$globe = new GlobeApi('v1');

	 $auth = $globe->auth(

 		"LGpj5IkA97jF5bibz8T9yAFzMpdjIqK6",
 		"dd642e5547d4c07c137c253538d76b4d065a0704c445b9e97f8ba6727a4d5787"

 	);

	 $sms = $globe->sms("1892");
	 $sms->sendMessage($subscriber['access_token'], $subscriber['subscriber_number'], $message);

}

?>