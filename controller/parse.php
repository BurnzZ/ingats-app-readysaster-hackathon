<?php


	function fetchData(){
		// $url = "http://www.pagasa.dost.gov.ph/files/cyclone.dat";
		$url = "http://localhost/hackathon-readysaster/cyclone.dat";

		// Set up curl
		$curl_handle=curl_init();
		curl_setopt($curl_handle,CURLOPT_URL, $url);
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		// Parse data to JSON
		if (!empty($buffer)){
			$buffer = preg_split("/[\s]+/", $buffer);
			$data['name'] = $buffer[0];
			$arr = array();
			for ($i=1; $i < count($buffer); $i++) {
				if(strlen($buffer[$i]) != 0){
					$line = explode(",", $buffer[$i]);
					$info = array();
					$info['type'] = $line[0];
					$info['date'] = $line[1];
					$info['time'] = $line[2];
					$info['long'] = $line[3];
					$info['lat'] = $line[4];
					$arr[$i-1] = $info;
				}
			}
			//header('Content-Type: application/json');
			$data['track'] = $arr;

			//echo json_encode($data);

			onDataFetched($data); //call event handler

			return $data;
		}
	}

	/**
	*	@param $long longtitude of subscriber
	*	@param $lat latitude of subscriber
	*/
	function getDistance($long, $lat, $cyclone){//distance in km
		return computeDistance($cyclone['longitude'],
								 $cyclone['latitude'],
								  $long,
								   $lat);
	}

	function computeDistance($longitude1, $latitude1, $longitude2, $latitude2){
		$theta = $longitude1 - $longitude2;
		$km = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$km = acos($km);
		$km = rad2deg($km);
		$km = $km * 60 * 1.1515;
		return $km*1.609344; 
	}

?>