<?php

	function fetchData($url){
		$curl_handle=curl_init();
		curl_setopt($curl_handle,CURLOPT_URL, $url);
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		return $buffer;	
	}

		$km = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$km = acos($km);
		$km = rad2deg($km);
		$km = $km * 60 * 1.1515;
		return $km*1.609344; 
	}

	//compute distance (in kilometers) of two coordinates
	function computeDistance($longitude1, $latitude1, $longitude2, $latitude2){
		$theta = $longitude1 - $longitude2;

	// lat, lng of the user's location
	function getNearestStation($lng, $lat){
		$data = json_decode(fetchData("http://202.90.153.89/api/stations"))[1]->stations;
		$index = -1;
		$minDistance = -1;
		
		// compute for the nearest station
		for ($i=0; $i < count($data); $i++) { 
			if($i==0){
				$minDistance = computeDistance($data[$i]->lng, $data[$i]->lat, $lng, $lat);
				$index = 0;
			} else {
				if(computeDistance($data[$i]->lng, $data[$i]->lat, $lng, $lat) < $minDistance ){
					$minDistance = computeDistance($data[$i]->lng, $data[$i]->lat, $lng, $lat);
					$index = $i;
				}
			}
		}

		if($index >= 0){
			return $data[$index]->url;
		}

	}

	//get max amount of rainfall in mm
	function getMaxAmountOfRainfall($lng, $lat){
		$base = "http://202.90.153.89";
		$url = $base.	aagetNearestStation($lng, $lat);		
		$data = json_decode(fetchData($url))->series;
		// 1 for rainfall
		return $data[1]->maxval;
	}

	// var_dump(getMaxAmountOfRainfall(123.64337158203125, 14.724417078179279));

?>