<?php

	/*
		relay.php is the final BRIDGE between the GLOBE API
		and the Ingats! app. This serves as the official contract
		of a subscriber, and the API as being "connected"

	*/

 	require ('globeAPI/src/GlobeApi.php');
 	require ("../db/Database.php");
 	$globe = new GlobeApi('v1');
 	$db = new Database();

 	session_start();

 	if(isset($_SESSION['location'])){

	 	$location = $_SESSION['location'];
	 	$longitude = $_SESSION['longitude'];
	 	$latitude  = $_SESSION['latitude'];


	 	$auth = $globe->auth(

	 		"LGpj5IkA97jF5bibz8T9yAFzMpdjIqK6",
	 		"dd642e5547d4c07c137c253538d76b4d065a0704c445b9e97f8ba6727a4d5787"

	 	);

	 	session_destroy();

	 	$code = $_GET['code'];
	 	$response1 = $auth->getAccessToken($code);
	 	$db->insertSubscriber($response1['access_token'],$response1['subscriber_number'],$longitude,$latitude,$location);
	 	
	 	$sms = $globe->sms("1892");
	 	$message = "You are now subscribed to Ingats. Your latest location is tagged from ".$location.". Stay tuned for the latest storm updates.";
	 	$response2 = $response = $sms->sendMessage($response1['access_token'], $response1['subscriber_number'], $message);
	 

 	}

 	else{

 		$auth = $globe->auth(

	 		"LGpj5IkA97jF5bibz8T9yAFzMpdjIqK6",
	 		"dd642e5547d4c07c137c253538d76b4d065a0704c445b9e97f8ba6727a4d5787"

	 	);

	 	$code = $_GET['code'];

	 	$response1 = $auth->getAccessToken($code);
	 	$db->insertSubscriber($response1['access_token'],$response1['subscriber_number'],121.01759800000002,14.559206,"Smart Communications - Smart Tower, Makati, Metro Manila, Philippines");

 	}




?>