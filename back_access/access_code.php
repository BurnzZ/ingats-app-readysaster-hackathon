<?php

	/*

		Files in back_access provides an AJAX interface between
		INGATS! and the SMS API, so that the API will be encapsulated.

		The SMS API is called using php cURL, and the scripts containing
		cURL are called via AJAX.

	*/


	/*

		access_code.php is STEP 1 of the subscription system.
		This calls the SMS API for an access key to Ingats! app.
		Any pertinent form details will come as well.

	*/

	require ("../db/Database.php");

	$number = $_POST['access_token']['subscriber_num'];

	$longitude = $_POST['longitude'];
	$latitude = $_POST['latitude'];
	$location  = $_POST['location'];
	if($db->isSubscriberExisting($number)){
		$db->updateSubscriber($number,$longitude,$latitude,$location);
		echo "";
	}

	else{

		$url = "http://developer.globelabs.com.ph/oauth/request_authorization";


		$field_string = http_build_query($_POST);

		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,2);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$field_string);
		$result = curl_exec($ch);
		curl_close($ch);

		//print_r($result);

	}

?>