<?php

class Database {
	public $con;
    const HOST = 'localhost';
    const USERNAME = 'root';
    const PASSWORD = '';
    const DATABASE_NAME = 'readysaster_db';

    // method declaration
    function __construct() {
		$this->con = mysql_connect(self::HOST,self::USERNAME,self::PASSWORD);

		$select_db = mysql_select_db(self::DATABASE_NAME);
		if(!$select_db){
			echo "Failed to select db: " . mysql_connect_error();
		}
		if (!$this->con) {
			echo "Failed to connect to MySQL: " . mysql_connect_error();
		}
    }

    public function getSubscribers(){
    	$query = "SELECT * FROM subscriber";

    	$result = mysql_query($query, $this->con);
    	if(!$result){
    		echo "Failed to get subscribers.";
    	}else{
    		$array_result = []; 

            while($row = mysql_fetch_array($result)){
                $array_result[] = $row;
            }
    		if(!$array_result) return [];
    		else return $array_result;
    	}
    }

    public function insertSubscriber($access_token, $subscriber_number, $longitude, $latitude, $location){
    	$query = "INSERT INTO subscriber (access_token, subscriber_number, longitude, latitude, location)
    		VALUES ('{$access_token}', '{$subscriber_number}', '{$longitude}', '{$latitude}', '{$location}')";

    	if(!mysql_query($query, $this->con)){
    		echo "Failed to insert subscriber";
    	}


    }

    public function deleteSubscriber($subscriber_number){
    	$query = "DELETE FROM subscriber WHERE subscriber_number='{$subscriber_number}'";
    	if(!mysql_query($query, $this->con)){
    		echo "Failed to delete subscriber";
    	}
    }

    public function updateSubscriber($subscriber_number, $longitude, $latitude, $location){
    	$query = "UPDATE subscriber SET subscriber_number='{$subscriber_number}', longitude='{$longitude}', latitude='{$latitude}', location='{$location}' WHERE subscriber_number='{$subscriber_number}'";

		echo $query;
		if(!mysql_query($query, $this->con)){
    		echo "Failed to update subscriber";
    	}
    }

	public function isSubscriberExisting($subscriber_number){
		$query = "SELECT id FROM subscriber WHERE subscriber_number='{$subscriber_number}'";

		$result = mysql_query($query, $this->con);
		
		$array_result = mysql_fetch_array($result);

		if($array_result){
			return true;
		}else return false;
	}

    public function getSubscriber($subscriber_number){
        $query = "SELECT * FROM subscriber WHERE subscriber_number='{$subscriber_number}'";

        $result = mysql_query($query, $this->con);

        return mysql_fetch_array($result);
    }
}

$db = new Database();

?>