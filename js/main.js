$(document).ready(function(){
	$('#unsubscribe-anchor').on('click',unsubscribe);
	$('#input-mobile-number').on('blur', retrieveSubscriptionData);

	$('#new_access_token').submit(registerUser);
	$("body").on("submit","form[class=edit_access_token]",function(event){
		confirmUser();
		return false;
	});

	$('div#title').on('click', function() {
		$('div.group').toggle(400);
	});
});

function toggleLabelTip() {
	$('label#hide-toggle').toggle();
}

var currentNumber = -1;

function retrieveSubscriptionData(){

	var number =  $('#input-mobile-number').val();

	$.ajax({
		url : 'controller/checkSubscription.php',
		type : "POST",
        data: "subscriber_number="+number,
		success : function(result){
			subscriberInfo = JSON.parse(result);
			if(currentNumber == -1){
				currentNumber = $('#input-mobile-number');
			}
			if(subscriberInfo && currentNumber != $('#input-mobile-number')){
				switchToUpdateButton();
				$('#input-longitude').val(subscriberInfo.longitude);
				$('#input-latitude').val(subscriberInfo.latitude);
				$('#pac-input').val(subscriberInfo.location);
				setMarker(new google.maps.LatLng(subscriberInfo.longitude, subscriberInfo.latitude));
			}else{
				if($('#subscribe-button').val() != "Subscribe!"){
					switchToSubscribeButton();
				}
			}
		}
	});
	return false;
}

function switchToUpdateButton(){
	$('#subscribe-button').val("Update");
	$('#subscribe-button').unbind('submit');
	$('#subscribe-button').submit(updateUser);
	currentNumber = $('#input-mobile-number');
}

function switchToSubscribeButton(){
	$('#subscribe-button').val("Subscribe!");
	$('#subscribe-button').unbind('submit');
	$('#subscribe-button').submit(registerUser);
	currentNumber = $('#input-mobile-number');	

}

function updateUser(){
	$.ajax({
		url : "controller/updateSubscriber.php",
		type : "POST",
        data: $('#new_access_token').serialize(),
		success : function(result){
			isSuccessful = JSON.parse(result);

			if(!isSuccessful){
				alert('failed to update information');
			}
		}
	});
}

function registerUser(){
	var formattedNumber = getFormattedNumber($('#input-mobile-number').val());
	console.log(formattedNumber);

	otherData = "latitude="+$("#input-longitude").val()+"&longitude="+$("#input-latitude").val()+"&location="+$("#pac-input").val();

	if(formattedNumber != null){
		var form = $('#new_access_token');

		$.ajax({
			url : "back_access/access_code.php",
			type : "POST",
	        data: "access_token[app_id]=601&access_token[subscriber_num]="+$("#input-mobile-number").val()+"&"+otherData,
	           
			success : onRegisterResultReceived
		});

	}else{
		alert("Invalid Mobile Number. Format should be in the following formats:\n+639XXXXXXXXX\n639XXXXXXXXX\n09XXXXXXXXX");
	}
	return false;
}

function confirmUser(){

	dataString1="latitude="+$("#input-longitude").val()+"&longitude="+$("#input-latitude").val()+"&location="+$("#pac-input").val();
	dataString2="access_token[confirmation_code]="+$("#access_token_confirmation_code").val()+"&access_token[app_id]="+$("#access_token_app_id").val()+"&access_token[subscriber_num]="+$("#input-mobile-number").val();
	dataString = dataString1 + "&" +dataString2;
		$.ajax({
			url : "back_access/confirm_auth.php",
			type : "POST",
	        data: dataString,
	           
			success : function(response){

				if(response==="ERROR"){
					toastr['error']("Wrong authentication code.");
					$('div#modal').trigger('closeModal');
					return;
				}


				console.log(response);
				finalURL = $($(response)[1]).prop("href");
				console.log(finalURL);
				$.get(finalURL,function(e){
					console.log(e);
					toastr['success']("Verified and subscribed successfully.");
					$('div#modal').trigger('closeModal');
					switchToUpdateButton();
				 });

			}
		});
}


function onRegisterResultReceived(result){
	console.log(result);
	var form = $(result).find("form").parent();

	if(form.children().length>0){

		console.log(form);

		$('div#modal').html(form);
		$('div#modal').trigger('openModal');
		$('.edit_access_token').attr("action","http://developer.globelabs.com.ph/oauth/confirm_authorization");

	}


	else{
		toastr['warning']("Already subscribed from before.");
	}

	//if(SUCCESS) onUserRegistered()
	//else
}


function onUserRegistered(){
	alert("Registration successful! You are now subscribe to alerts.");
}

function unsubscribe() {
	do{
		var number = prompt("Enter Mobile Number to unsubscribe: ");
		number = getFormattedNumber(number)

		if (number != null) {
			$.ajax({
				url : "controller/unsubscribeUser.php",
				type : "POST",
		        data: "subscriber_number="+number,
		           
				success : function(result){
					alert('You are now unsubscribe from our service');
				}
			});
		}
		else {
			alert('invalid username and password');
		}
	}while(number != "" && number !== null);
	
}

function getFormattedNumber(number){
	if(number == null) return null;
	if(number.match(/^(\+63|63|0)?9[0-9]{9}$/)){
		return number.substring(number.indexOf("9"),number.length);
	}
	return null;
}