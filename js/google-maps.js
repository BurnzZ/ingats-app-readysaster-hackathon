var geocoder;
var map;
var marker;
var mouseDowned = false;
var mouseMoved = false;
var mouseMoveCount = 0;
var infowindow;

function initialize() {
 
   	var gitna = new google.maps.LatLng(12, 120.97388);
    var mapOptions = {
    	center: gitna,
		zoom: 6
    };

	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

	geocoder = new google.maps.Geocoder(); //used for getting the name of the place based on map coordinates

	marker = new google.maps.Marker({
		position: null, 
		map: map
	});

	google.maps.event.addListener(map, 'mousemove', function(event){
		mouseMoveCount++;
		if(mouseMoveCount > 5){
			mouseDowned = false;
		}
	});

	google.maps.event.addListener(map, 'mouseup', function(event){
		if(mouseDowned){
			updateLocation(event.latLng);    
		}

		mouseDowned = false;
		mouseMoved = false;
		mouseMoveCount = 0;
	});

	google.maps.event.addListener(map, 'mousedown', function(event){
		mouseDowned = true;
		mouseMoved = false;
		mouseMoveCount = 0;
	});

	/* This section is for the autocomplete search bar of the maps */
	var input = document.getElementById('pac-input');
	var autocomplete = new google.maps.places.Autocomplete(input); 

	infowindow = new google.maps.InfoWindow();

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		infowindow.close();
		marker.setVisible(false);
		var place = autocomplete.getPlace();
		
		if (!place.geometry) {
			return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}
		
		marker.setIcon(/** @type {google.maps.Icon} */({
			url: place.icon,
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(35, 35)
		}));
		setMarker(place.geometry.location);
		setLatLng(place.geometry.location);
		marker.setVisible(true);

		var address = '';
		if (place.address_components) {
			address = [
			(place.address_components[0] && place.address_components[0].short_name || ''),
			(place.address_components[1] && place.address_components[1].short_name || ''),
			(place.address_components[2] && place.address_components[2].short_name || '')
			].join(' ');
		}

		infowindow.open(map, marker);
	});
	/* End of the Autocomplete Search input */
}

google.maps.event.addDomListener(window, 'load', initialize);

function setMarker(latLng){
	marker.setPosition(latLng);
}

//request geocode information of a coordinate and update relevant location info after data receive
function updateLocation(latLng){
	geocoder.geocode({'latLng': latLng}, function(results, status){
		onLocationNameReceived(results, status, latLng);
	});
}

//set location info from the UI
function onLocationNameReceived(results, status, latLng){
	setMarker(latLng);
	if (status == google.maps.GeocoderStatus.OK) {
		if (results[1]) {
			setLatLng(latLng);
			setLocationName(results[1].formatted_address);
		}
	} else {
		$('#input-longitude').val("");
   		$('#input-latitude').val("");	
   		$('#pac-input').val("");
   		infowindow.setContent('<div>Unknown Location');
	}
	
}

function setLatLng(latLng){
	$('#input-longitude').val(latLng.lng());
    $('#input-latitude').val(latLng.lat());	
}

function setLocationName(locationName){
	var location = locationName.substr(0,locationName.indexOf(","));
	$('#pac-input').val(locationName);

	console.log(infowindow);
	infowindow.close();
	if(location == ""){
		infowindow.setContent('<div><strong>' + locationName + '</strong>');
	}else{
		infowindow.setContent('<div><strong>' + location + '</strong><br>' + locationName);
	}
	infowindow.open(map,marker);
}	
